package com.example.test1;

import com.google.gson.annotations.SerializedName;

public class WeatherItem {
    @SerializedName("title")
    private String city;
    @SerializedName("location_type")
    private String type;
    private String woeid;
    @SerializedName("latt_long")
    private String location;

    public String getCity() {
        return city;
    }

    public String getType() {
        return type;
    }

    public String getWoeid() {
        return woeid;
    }

    public String getLocation() {
        return location;
    }
}
