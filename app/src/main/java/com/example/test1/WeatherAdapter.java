package com.example.test1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class WeatherAdapter extends ListAdapter<WeatherItem, WeatherAdapter.WeatherViewHolder> {

    protected WeatherAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<WeatherItem> DIFF_CALLBACK =new DiffUtil.ItemCallback<WeatherItem>() {
        @Override
        public boolean areItemsTheSame(@NonNull WeatherItem oldItem, @NonNull WeatherItem newItem) {
            return false;
        }

        @Override
        public boolean areContentsTheSame(@NonNull WeatherItem oldItem, @NonNull WeatherItem newItem) {
            return false;
        }
    };

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false);
        return new WeatherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        WeatherItem weatherItem = getItem(position);
        holder.tvCity.setText(weatherItem.getCity());
        holder.tvWoeid.setText(weatherItem.getWoeid());
        holder.tvLocation.setText(weatherItem.getLocation());
    }

    class WeatherViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCity;
        private TextView tvWoeid;
        private TextView tvLocation;

        public WeatherViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCity = itemView.findViewById(R.id.title);
            tvWoeid = itemView.findViewById(R.id.woeid);
            tvLocation = itemView.findViewById(R.id.location);
        }
    }
}
