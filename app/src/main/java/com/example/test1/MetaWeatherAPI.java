package com.example.test1;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MetaWeatherAPI {

    @GET("location/search/?query=ba")
    Call<List<WeatherItem>> getWeatherItems();
}
