package com.example.test1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private WeatherAdapter weatherAdapter;
    private List<WeatherItem> weatherItemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //drawer
        Toolbar toolbar = findViewById(R.id.toolbar);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //list
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        weatherAdapter = new WeatherAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(weatherAdapter);
        getWeatherData();
        implementSearch();
    }

    private void getWeatherData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.metaweather.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MetaWeatherAPI metaWeatherAPI = retrofit.create(MetaWeatherAPI.class);
        Call<List<WeatherItem>> call = metaWeatherAPI.getWeatherItems();
        call.enqueue(new Callback<List<WeatherItem>>() {
            @Override
            public void onResponse(Call<List<WeatherItem>> call, Response<List<WeatherItem>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Connection error\n" + response.code(), Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "" + response.code());
                    return;
                }

                weatherItemList = response.body();
                weatherAdapter.submitList(weatherItemList);
                findViewById(R.id.loading).setVisibility(GONE);
            }

            @Override
            public void onFailure(Call<List<WeatherItem>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "" + t.getMessage());
            }
        });
    }
//test
    private void implementSearch() {
        EditText etSearch = findViewById(R.id.search);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                List<WeatherItem> fullList = weatherItemList;
                ArrayList<WeatherItem> filteredList = new ArrayList<>();
                for (WeatherItem item : fullList) {
                    if (item.getCity().toLowerCase().contains(s.toString()))
                        filteredList.add(item);
                }
                weatherAdapter.submitList(filteredList);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}